# Scalliony API samples

Various ways to create scalliony compatible programs.

See README.md in each folders

## Languages

- [C/C++](clang/README.md)
- [Rust](rust/README.md)
- [AssemblyScript](assemblyscript/README.md): A subset of TypeScript
- [WAT](wat/README.md): Plain text assembler

## Tips

Wasm files can be optimized using [binaryen](https://github.com/webassembly/binaryen)'s `wasm-opt -O raw.wasm -o opt.wasm`

Wasm files can be converted back to *readable* [WAT](https://webassembly.github.io/spec/core/text/index.html) using [wabt](https://github.com/WebAssembly/wabt)'s `wasm2wat`

## API

Full description is available as [api.json](api.json).

TODO: auto generate doc

### Multi-value return

WebAssembly [multi-value proposal](https://github.com/WebAssembly/multi-value) defines a way to return a tuple of mixed type values from function calls.
This feature is fully supported by [wasmtime](https://wasmtime.dev/), the WASM runtime used by Scalliony. Sadly any compiler is well supported for now.

As fallback, *C-like* functions `_s` are temporary available and return original struct value at a user provider pointer address.

## License

Distributed under the MIT License. See [LICENSE](LICENSE) for more information.