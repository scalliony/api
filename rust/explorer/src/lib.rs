#![allow(unused)]
extern crate scalliony;
use scalliony::*;

fn main() {
/// Called at boot-time (optional)
#[no_mangle]
pub extern "C" fn _start() {
    console::log("Explorer");
}

/// Called at each tick (required)
#[no_mangle]
pub extern "C" fn run() {
    match sensors::contact() {
        Some(_) => motor::rotate_left(),
        None => {},
    }
    motor::go_forward(3);
}
}