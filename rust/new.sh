#!/bin/bash
cp -r ./hello ./$1
sed "s/hello/$1/" "./$1/Cargo.toml" > "./$1/Cargo.tpl" && mv "./$1/Cargo.tpl" "./$1/Cargo.toml"
sed "s/\",]/\", \"$1\",]/" "Cargo.toml" > "Cargo.tpl" && mv "Cargo.tpl" "Cargo.toml"