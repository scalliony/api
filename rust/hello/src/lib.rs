#![allow(unused)]
extern crate scalliony;
use scalliony::*;

fn main() {
/// Called at boot-time (optional)
#[no_mangle]
pub extern "C" fn _start() {
    console::log("Starting");
}

/// Called at each tick (required)
#[no_mangle]
pub extern "C" fn run() {
    console::log("Hello, scalliony!");
}
}