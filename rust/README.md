# Rust

Just a simple rust binding

## Content

- [api](./api): Scalliony API Rust bindning
- [hello](./hello/src/lib.rs): Simple `Hello world`
- [explorer](./explorer/src/lib.rs): Stupid traveler

## Install

Requires [rustup](https://rustup.rs/)

1. Install wasm32 target
```sh
rustup target add wasm32-unknown-unknown
```
2. Copy this folder


## Setup

Note: you could also directly use `hello` project

Create new project `<name>`
Run `./new.sh <name>`

Or manually
1. Copy `hello` folder as `<name>`
2. Change `name` value of `<name>/Cargo.toml`
3. Add `"<name>",` to `members` array of `Cargo.toml`

Update `<name>/src/lib.rs`

## Build

```sh
cargo wasm -p <name>
```
Omit `-p <name>` to build all projects
```sh
# -- or manually
cargo build --target wasm32-unknown-unknown --release -p <name>
```

Upload `target/wasm32-unknown-unknown/release/<name>.wasm`