pub enum EntityType {
    Unexpected = -1, Rock, Bot, Building
}
impl EntityType {
    fn from_i32(value: i32) -> EntityType {
        match value {
            0 => EntityType::Rock,
            1 => EntityType::Bot,
            2 => EntityType::Building,
            _ => EntityType::Unexpected,
        }
    }
}

pub enum Rotation {
    Up, Right, Down, Left
}
impl Rotation {
    fn from_i32(value: i32) -> Rotation {
        match value {
            1 => Rotation::Right,
            2 => Rotation::Down,
            3 => Rotation::Left,
            _ => Rotation::Up,
        }
    }
}

pub struct Entity {
    pub id: i64,
    pub typ: EntityType,
}
#[repr(C)]
pub struct Cell {
    pub x: i32,
    pub y: i32,
}

pub mod console {
    #[link(wasm_import_module = "console")]
    extern "C" {
        /// imports console.log
        #[link_name = "log"]
        fn raw_console_log(s: *const u8, l: u32);
    }

    /// Write string to logs
    pub fn log(s: &str) {
        unsafe { raw_console_log(s.as_ptr(), s.len() as u32); }
    }
}

/// Write string formatted to logs
/// Warning: formatting is not cheap
#[macro_export]
macro_rules! log {
    ($($t:tt)*) => (api::console::log(&format_args!($($t)*).to_string()))
}

pub mod env {
    #[link(wasm_import_module = "env")]
    extern "C" {
        /// imports env.chunk_scale
        #[link_name = "chunk_scale"]
        fn raw_env_chunk_scale() -> u32;

        /// imports env.bot_id
        #[link_name = "bot_id"]
        fn raw_env_bot_id() -> i64;

        /// imports env.program_id
        #[link_name = "program_id"]
        fn raw_env_program_id() -> i32;
    }

    /// Get the chunk_size power of 2
    pub fn chunk_scale() -> u32 {
        unsafe { raw_env_chunk_scale() }
    }
    /// Get the chunk_size
    pub fn chunk_size() -> u32 {
        1 << chunk_scale()
    }

    /// Get own entity id
    pub fn id() -> i64 {
        unsafe { raw_env_bot_id() }
    }
    /// Get own program id
    pub fn program_id() -> i32 {
        unsafe { raw_env_program_id() }
    }
}

pub mod motor {
    #[link(wasm_import_module = "motor")]
    extern "C" {
        /// imports motor.rotate
        #[link_name = "rotate"]
        fn raw_motor_rotate(left: i32);

        /// imports motor.move
        #[link_name = "move"]
        fn raw_motor_move(dist: i32);
    }

    pub fn rotate(left: bool) {
        unsafe { raw_motor_rotate(match left {
            true => -1,
            false => 1,
        }); }
    }
    pub fn rotate_left() {
        rotate(true);
    }
    pub fn rotate_right() {
        rotate(false);
    }

    /// Move forward of dist cells
    /// Direction depends of current rotation
    /// Actual movement is delayed
    pub fn go_forward(dist: u16) {
        unsafe { raw_motor_move(dist as i32); }
    }
}

pub mod sensors {
    #[repr(C)]
    struct Entity {
        id: i64,
        type_id: i32,
    }
    impl Entity {
        fn as_pub(&self) -> Option<crate::Entity> {
            match crate::EntityType::from_i32(self.type_id) {
                crate::EntityType::Unexpected => None,
                typ if self.id > 0 => Some(crate::Entity{ id: self.id, typ }),
                _ => None
            }
        }
    }

    #[link(wasm_import_module = "sensors")]
    extern "C" {
        /// imports sensors.contact_s
        #[link_name = "contact_s"]
        fn raw_sensors_contact() -> Entity;

        /// imports sensors.gyroscope
        #[link_name = "gyroscope"]
        fn raw_sensors_gyroscope() -> i32;

        /// imports sensors.radar_s
        #[link_name = "radar_s"]
        fn raw_sensors_radar(relative: crate::Cell) -> Entity;

        /// imports sensors.map_s
        #[link_name = "map_s"]
        fn raw_sensors_map(at: crate::Cell) -> Entity;

        /// imports sensors.gps_s
        #[link_name = "gps_s"]
        fn raw_sensors_gps() -> crate::Cell;
    }

    /// Check for entity just in front (depending of rotation)
    /// Returns entity if something is in contact
    pub fn contact() -> Option<crate::Entity> {
        unsafe { raw_sensors_contact().as_pub() }
    }

    /// Get current rotation
    pub fn gyroscope() -> crate::Rotation {
        unsafe { crate::Rotation::from_i32(raw_sensors_gyroscope()) }
    }

    /// Check for entity at relative position
    /// Returns entity if position is not empty
    /// Querying out of chunk_size radius may produce an error
    pub fn radar(relative: crate::Cell) -> Option<crate::Entity> {
        unsafe { raw_sensors_radar(relative).as_pub() }
    }

    /// Check for entity at absolute position
    /// Returns entity if position is not empty
    /// Querying out of chunk_size radius around actual position may produce an error
    pub fn map(at: crate::Cell) -> Option<crate::Entity> {
        unsafe { raw_sensors_map(at).as_pub() }
    }

    /// Get absolute position
    pub fn gps() -> crate::Cell {
        unsafe { raw_sensors_gps() }
    }

}