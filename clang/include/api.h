#include "raw.h"
#include "nostdlib.h"
#include <stdbool.h>

struct str_t {
    const char *data;
    uint32_t size;
};
/// Write string to logs
inline void console_log(const char *str) {
    _console_log(str, strlen(str));
}
inline void console_log_n(const struct str_t *str) {
    _console_log(str->data, str->size);
}

inline int puts(const char* str) {
    console_log(str);
    return 0;
}
inline int puts_n(const char* str, size_t len) {
    _console_log(str, len);
    return 0;
}

/// Get the chunk_size power of 2
inline uint32_t chunk_scale() {
    return _env_chunk_scale();
}
/// Get the chunk_size
inline uint32_t chunk_size() {
    return 1u << chunk_scale();
}

/// Get own entity id
inline int64_t id() {
    return _env_bot_id();
}
/// Get own program id
inline int32_t program_id() {
    return _env_program_id();
}


inline void motor_rotate(bool left) {
    _motor_rotate(left ? -1 : 1);
}
inline void motor_rotate_left() {
    motor_rotate(true);
}
inline void motor_rotate_right() {
    motor_rotate(false);
}

/// Move forward of dist cells
/// Direction depends of current rotation
/// Actual movement is delayed
inline void motor_move(uint16_t dist) {
    _motor_move(dist);
}

enum entity_type: int32_t {
    Unexpected = -1,
    Rock, Bot, Building
};
struct entity_t {
    int64_t id;
    enum entity_type type;
};
enum rotation: int32_t {
    Up = 0, Right, Down, Left
};
struct cell_t {
    int32_t x, y;
};

/// Check of returned entity is valid
inline bool is_valid_entity(struct entity_t e) {
    return e.id > 0 && e.type != Unexpected;
}

/// Check for entity just in front (depending of rotation)
/// Returns valid entity if something is in contact
/// Must check validity with bool is_valid_entity(struct entity_t)
inline struct entity_t sensors_contact() {
    struct i64_32_t res = _sensors_contact();
    return *(struct entity_t*)&res;
}

/// Get current rotation
inline enum rotation sensors_gyroscope() {
    return (enum rotation)_sensors_gyroscope();
}

/// Check for entity at relative position
/// Returns valid entity if position is not empty
/// Must check validity with bool is_valid_entity(struct entity_t)
/// Querying out of chunk_size radius may produce an error
inline struct entity_t sensors_radar(struct cell_t relative) {
    struct i64_32_t res = _sensors_radar(relative.x, relative.y);
    return *(struct entity_t*)&res;
}

/// Check for entity at absolute position
/// Returns valid entity if position is not empty
/// Must check validity with bool is_valid_entity(struct entity_t)
/// Querying out of chunk_size radius around actual position may produce an error
inline struct entity_t sensors_map(struct cell_t absolute) {
    struct i64_32_t res = _sensors_map(absolute.x, absolute.y);
    return *(struct entity_t*)&res;
}

/// Get absolute position
inline struct cell_t sensors_gps() {
    struct i32_32_t res = _sensors_gps();
    return *(struct cell_t*)&res;
}