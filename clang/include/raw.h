#include <stdint.h>

struct i64_32_t {
    int64_t a;
    int32_t b;
};
struct i32_32_t {
    int32_t a, b;
};

void _console_log(const char *s, int32_t l) __attribute__((
    __import_module__("console"),
    __import_name__("log")));

uint32_t _env_chunk_scale() __attribute__((
    __import_module__("env"),
    __import_name__("chunk_scale")
));
int64_t _env_bot_id() __attribute__((
    __import_module__("env"),
    __import_name__("bot_id")
));
int32_t _env_program_id() __attribute__((
    __import_module__("env"),
    __import_name__("program_id")
));

void _motor_rotate(int32_t left) __attribute__((
    __import_module__("motor"),
    __import_name__("rotate")
));
void _motor_move(int32_t dist) __attribute__((
    __import_module__("motor"),
    __import_name__("move")
));

struct i64_32_t _sensors_contact() __attribute__((
    __import_module__("sensors"),
    __import_name__("contact_s")
));
int32_t _sensors_gyroscope() __attribute__((
    __import_module__("sensors"),
    __import_name__("gyroscope")
));
struct i64_32_t _sensors_radar(int32_t x, int32_t y) __attribute__((
    __import_module__("sensors"),
    __import_name__("radar_s")
));
struct i64_32_t _sensors_map(int32_t x, int32_t y) __attribute__((
    __import_module__("sensors"),
    __import_name__("map_s")
));
struct i32_32_t _sensors_gps() __attribute__((
    __import_module__("sensors"),
    __import_name__("gps_s")
));