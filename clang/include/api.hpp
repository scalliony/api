#include "raw.h"
#include "nostdlib.hpp"

namespace console {
    /// Write string to logs
    inline void log(const std::string_view &str) {
        _console_log(str.c_str(), str.size());
    }
}

namespace env {
    /// Get the chunk_size power of 2
    inline uint32_t chunk_scale() {
        return _env_chunk_scale();
    }
    /// Get the chunk_size
    inline uint32_t chunk_size() {
        return 1u << chunk_scale();
    }

    /// Get own entity id
    inline int64_t id() {
        return _env_bot_id();
    }
    /// Get own program id
    inline int32_t program_id() {
        return _env_program_id();
    }
}

namespace motor {
    inline void rotate(bool left) {
        _motor_rotate(left ? -1 : 1);
    }
    inline void rotate_left() {
        rotate(true);
    }
    inline void rotate_right() {
        rotate(false);
    }

    /// Move forward of dist cells
    /// Direction depends of current rotation
    /// Actual movement is delayed
    inline void move(uint16_t dist) {
        _motor_move(dist);
    }
}


enum class entity_type: int32_t {
    Rock, Bot, Building
};
struct entity_t {
    int64_t id;
    entity_type type;

    /// Check and return entity if valid
    static inline std::optional<entity_t> Of(i64_32_t raw) {
        if (raw.a > 0 && raw.b >= 0)
            return entity_t{raw.a, static_cast<entity_type>(raw.b)};
        else
            return {};
    }
};
enum class rotation: int32_t {
    Up = 0, Right, Down, Left
};
struct cell_t {
    int32_t x, y;

    static inline cell_t Of(i32_32_t raw) {
        return cell_t{raw.a, raw.b};
    }
};

namespace sensors {
    /// Check for entity just in front (depending of rotation)
    /// Returns entity if something is in contact
    inline std::optional<entity_t> contact() {
        return entity_t::Of(_sensors_contact());
    }

    /// Get current rotation
    inline rotation gyroscope() {
        return (rotation)_sensors_gyroscope();
    }

    /// Check for entity at relative position
    /// Returns entity if position is not empty
    /// Querying out of chunk_size radius may produce an error
    inline std::optional<entity_t> radar(cell_t relative) {
        return entity_t::Of(_sensors_radar(relative.x, relative.y));
    }

    /// Check for entity at absolute position
    /// Returns entity if position is not empty
    /// Querying out of chunk_size radius around actual position may produce an error
    inline std::optional<entity_t> map(struct cell_t absolute) {
        return entity_t::Of(_sensors_map(absolute.x, absolute.y));
    }

    /// Get absolute position
    inline cell_t gps() {
        return cell_t::Of(_sensors_gps());
    }
}