import { console, motor, sensors } from "./api"

/// Called at boot-time (optional)
export function _start(): void {
  console.log('Starting')
}

/// Called at each tick (required)
export function run(): void {
  const front = sensors.contact()
  if (front.isValid()) {
    motor.rotate_left()
  }
  motor.move(2)
}