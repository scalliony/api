declare namespace raw {
    @external("console", "log")
    function console_log(s: i32, l: i32): void

    @external("env", "chunk_scale")
    function env_chunk_scale(): u32;
    @external("env", "bot_id")
    function env_bot_id(): i64;
    @external("env", "program_id")
    function env_program_id(): i32;

    @external("motor", "rotate")
    function motor_rotate(left: i32): void
    @external("motor", "move")
    function motor_move(dist: i32): void

    @external("sensors", "contact_s")
    function sensors_contact(ret: Entity): void
    @external("sensors", "gyroscope")
    function sensors_gyroscope(): i32
    @external("sensors", "radar_s")
    function sensors_radar(relative: Cell, ret: Entity): void
    @external("sensors", "map_s")
    function sensors_map(at: Cell, ret: Entity): void
    @external("sensors", "gps_s")
    function sensors_gps(ret: Cell): void;
}

enum EntityType { Rock, Bot, Building }
enum Rotation { Up, Right, Down, Left }

@unmanaged
class Entity {
    id: i64;
    typ: u16;

    isValid(): bool {
        return this.id > 0 && this.typ >= 0
    }
    getType(): EntityType {
        return changetype<EntityType>(this.typ)
    }
}

@unmanaged
class Cell {
    x: i32;
    y: i32;
}

export namespace console {
    /** Write string to logs without encoding (ascii only) */
    export function log(s: string): void {
        raw.console_log(changetype<i32>(s), changetype<i32>(s.length * 2))
    }
    /** Write string to logs with encoding */
    export function log_utf8(s: string): void {
        const s8 = String.UTF8.encode(s)
        raw.console_log(changetype<i32>(s8), changetype<i32>(s8.byteLength))
    }
}

export namespace env {
    /** Get the chunk_size power of 2*/
    export function chunk_scale(): u32 {
        return raw.env_chunk_scale()
    }
    /** Get the chunk_size*/
    export function chunk_size(): u32 {
        return 1 << chunk_scale()
    }

    /** Get own entity id*/
    export function id(): i64 {
        return raw.env_bot_id()
    }
    /** Get own program id*/
    export function program_id(): i32 {
        return raw.env_program_id();
    }
}

export namespace motor {
    export function rotate(left: boolean): void {
        raw.motor_rotate(left ? -1 : 1)
    }
    export function rotate_left(): void {
        rotate(true)
    }
    export function rotate_right(): void {
        rotate(false)
    }

    /** Move forward of dist cells
      * Direction depends of current rotation
      * Actual movement is delayed */
    export function move(dist: u16): void {
        raw.motor_move(dist as i32)
    }
}

export namespace sensors {
    /** Check for entity just in front (depending of rotation)
      * Returns entity if something is in contact */
    export function contact(): Entity {
        let ret = new Entity()
        raw.sensors_contact(ret)
        return ret
    }

    /** Get current rotation */
    export function gyroscope(): Rotation {
        return changetype<Rotation>(raw.sensors_gyroscope())
    }

    /** Check for entity at relative position
      * Returns entity if position is not empty
      * Querying out of chunk_size radius may produce an error */
    export function radar(relative: Cell): Entity {
        let ret = new Entity()
        raw.sensors_radar(relative, ret)
        return ret
    }

    /** Check for entity at absolute position
      * Returns entity if position is not empty
      * Querying out of chunk_size radius around actual position may produce an error */
    export function map(at: Cell): Entity {
        let ret = new Entity()
        raw.sensors_map(at, ret)
        return ret
    }

    /** Get absolute position */
    export function gps(): Cell {
        let ret = new Cell()
        raw.sensors_gps(ret)
        return ret
    }
}